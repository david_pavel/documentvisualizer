package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.flicker;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.Application;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images.ImageService;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images.ImageType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { Application.class })
public class FlickerServiceTest {
	
	@Autowired
	@Qualifier("flickerService")
	private ImageService flickerService;

	@Test
	public void testFind() {
		final String query = "Australia flag";
		List<String> foundImages = flickerService.findImages(query, ImageType.CLIPART);
		
		foundImages.forEach(System.out::println);
		
		assertNotNull(foundImages);		
		assertTrue(foundImages.size() > 0);
		
	}
}
