package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.flicker;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.configuration.ImagesSearchConfig;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images.ImageService;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images.ImageType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class)
public class GoogleImageSearchServiceTest {
	
	@Configuration
	@Import(ImagesSearchConfig.class)
	@ComponentScan(basePackageClasses=ImageService.class)
    static class ContextConfiguration { }
	
	@Autowired
	@Qualifier("googleAjaxImageSearchService")
	private ImageService imagesService;

	@Test
	public void testFind() {
		final String query = "Australia flag";
		List<String> foundImages = imagesService.findImages(query, ImageType.CLIPART);
		
		foundImages.forEach(System.out::println);
		
		assertNotNull(foundImages);		
		assertTrue(foundImages.size() > 0);		
	}
}
