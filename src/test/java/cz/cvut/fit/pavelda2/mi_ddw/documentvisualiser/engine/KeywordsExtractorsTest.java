package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine;

import static cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine.KeywordsExtractionEngineType.ANNIE_GAZETTEER_ENGINE;
import static cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine.KeywordsExtractionEngineType.ANNIE_POS_TAGGER_ENGINE;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.configuration.GateConfig;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.KeywordOccurence;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { GateConfig.class })
public class KeywordsExtractorsTest {

	@Autowired
	private KeywordsExtractor extractor;

	@Test
	public void simpleExtraction() {
		final String text = SampleTexts.SHORT_TEXT;
		final List<KeywordOccurence> keywords = extractor.extractKeywords(text,
				KeywordsExtractionEngineType.createList(ANNIE_GAZETTEER_ENGINE,ANNIE_POS_TAGGER_ENGINE));
		
		System.out.println(keywords);
		assertNotNull(keywords);
		assertTrue(keywords.size() > 0);
	}
}
