package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service;

import java.awt.Point;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class SandboxTest {

	@Test
	public void testStream() {
		List<Point> strings = Arrays.asList(new Point[] {new Point(0, 0), new Point(1, 2)});

		strings.forEach(System.out::println);
		strings.parallelStream()
			.limit(1)
			.forEach(s -> s.setLocation(3, 3));
		
		strings.forEach(System.out::println);
		
	}
}
