package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service;

import static cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine.KeywordsExtractionEngineType.ANNIE_GAZETTEER_ENGINE;
import static cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine.KeywordsExtractionEngineType.ANNIE_POS_TAGGER_ENGINE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.Application;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.Document;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.TextSource;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine.SampleTexts;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { Application.class })
@TestPropertySource(properties="spring.jpa.hibernate.ddl-auto=create")
@TransactionConfiguration(defaultRollback = true)
public class DocumentServiceTest {

	@Autowired
	private DocumentService documentService;

	@Test
	public void testSave() {
		final String expectedText = SampleTexts.SHORT_TEXT;
		final String expectedCategory = "FariTales";
		final TextSource textSource = new TextSource();
		textSource.setCategory(expectedCategory);
		textSource.setText(expectedText);
		textSource.setExtractorTypes(ANNIE_GAZETTEER_ENGINE, ANNIE_POS_TAGGER_ENGINE);
		
		Document storedDocument = documentService.storeText(textSource);
		
		System.out.println(storedDocument);
		assertNotNull(storedDocument);		
		assertNotNull(storedDocument.getCategory());
		assertEquals(expectedCategory,storedDocument.getCategory().getName());
		assertEquals(expectedText,storedDocument.getText());
		assertNotNull(storedDocument.getKeywords());
		assertTrue(storedDocument.getKeywords().size() > 0);
		
	}
}
