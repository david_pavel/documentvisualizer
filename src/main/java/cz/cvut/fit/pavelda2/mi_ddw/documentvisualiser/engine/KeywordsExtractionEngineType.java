package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.WordUtils;

public enum KeywordsExtractionEngineType {
	OPEN_NLP_NER_ENGINE,
	ANNIE_POS_TAGGER_ENGINE,
	ANNIE_GAZETTEER_ENGINE;

	public static String getBeanName(KeywordsExtractionEngineType e) {
		
		String name = WordUtils
			.capitalizeFully(e.name(), new char[]{'_'})
			.replaceAll("_", "");
		return name.substring(0,1).toLowerCase() + name.substring(1);
	}
	
	public static List<KeywordsExtractionEngineType> createList(KeywordsExtractionEngineType ... types) {
		return Arrays.asList(types);
	}
	
	
}
