package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.configuration;

import gate.CreoleRegister;
import gate.Gate;
import gate.util.GateException;

import java.io.File;
import java.net.MalformedURLException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@ComponentScan("cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine")
@PropertySource("classpath:application.properties")
public class GateConfig {
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	   return new PropertySourcesPlaceholderConfigurer();
	}
		
	@Value("${gate.home}")
	private String GATE_HOME = "c:\\Program Files\\GATE_Developer_8.0\\";

	@Bean(name = "gateHome")
	public String getGateHome() {
		return GATE_HOME;
	}

	@PostConstruct
	public void init() throws GateException, MalformedURLException {
		File gateHomeFile = new File(getGateHome());
		File pluginsHome = new File(getGateHome() + "plugins");
		File annieHome = new File(pluginsHome, "ANNIE");
		File openNlpHome = new File(pluginsHome, "OpenNLP");

		Gate.setGateHome(gateHomeFile);
		Gate.setPluginsHome(pluginsHome);
		Gate.init();

		CreoleRegister register = Gate.getCreoleRegister();
		register.registerDirectories(annieHome.toURI().toURL());
		register.registerDirectories(openNlpHome.toURI().toURL());
	}

}
