package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.KeywordOccurence;

@Component
public class SimpleKeywordsExtractor implements KeywordsExtractor {

	private final Logger looger = LoggerFactory.getLogger(getClass());

	@Autowired
	ApplicationContext ctx;
		
	@Override
	public List<KeywordOccurence> extractKeywords(String text, List<KeywordsExtractionEngineType> engineTypes) {
		List<KeywordOccurence> keywords = new CopyOnWriteArrayList<>();
		
		// Extract keywords from all engines
		engineTypes.parallelStream().forEach(e -> {
			KeywordsExtractionEngine engine = ctx.getBean(KeywordsExtractionEngineType.getBeanName(e),KeywordsExtractionEngine.class);
			List<KeywordOccurence> extractKeywords = engine.extractKeywords(text);
			keywords.addAll(extractKeywords);
		});
				
		looger.debug("Extracted keywords: " + keywords.toString());
		return keywords;		
	}

}
