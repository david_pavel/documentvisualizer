package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.Document;

public interface DocumentRepository extends CrudRepository<Document, Long> {

	List<Document> findAllByCategoryName(String categoryName);
	List<Document> findAll();

}
