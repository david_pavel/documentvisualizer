package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages={"cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser"})
@ComponentScan
public class Application {

	public static void main(String[] args) {
		new SpringApplicationBuilder()
			.showBanner(false)
			.sources(Application.class)
			.run(args);
	}

}
