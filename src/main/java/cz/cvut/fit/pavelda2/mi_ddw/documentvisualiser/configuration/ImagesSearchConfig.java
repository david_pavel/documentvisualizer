package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.xml.xpath.Jaxp13XPathTemplate;
import org.springframework.xml.xpath.XPathOperations;

@Configuration
public class ImagesSearchConfig {
	
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@Bean
	public XPathOperations getXpAthTemplate() {
		return new Jaxp13XPathTemplate();
	}

}
