package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.thymeleaf.dialect;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.servlet.support.RequestContext;
import org.thymeleaf.Arguments;
import org.thymeleaf.dom.Element;
import org.thymeleaf.processor.attr.AbstractAttributeModifierAttrProcessor;

public class DecideMenuIsActiveProcessor extends
		AbstractAttributeModifierAttrProcessor {

	protected DecideMenuIsActiveProcessor() {
		super("active");
	}

	@Override
	public int getPrecedence() {
		return 1000;
	}

	@Override
	protected ModificationType getModificationType(Arguments arguments,
			Element element, String attributeName, String newAttributeName) {
        return ModificationType.APPEND_WITH_SPACE;
	}

	protected Map<String, String> getModifiedAttributeValues(
            final Arguments arguments, final Element element, final String attributeName) {

        final String urlPrefix = element.getAttributeValue(attributeName);

        final Map<String,String> values = new HashMap<String, String>();
        RequestContext requestContext = (RequestContext) arguments.getContext().getVariables().get("springRequestContext");
        final String uri = requestContext.getRequestUri();
        final String uriWithoutContextPath = uri.substring(requestContext.getContextPath().length());
        
        if (urlPrefix.startsWith("exact:")) {
        	if (uriWithoutContextPath.equalsIgnoreCase(urlPrefix.substring(6))) {
            	values.put("class", "active"); 
        	}
        } else if (uriWithoutContextPath.startsWith(urlPrefix)) { 
        	values.put("class", "active"); 
        } 

        return values;
    }

	@Override
	protected boolean recomputeProcessorsAfterExecution(Arguments arguments,
			Element element, String attributeName) {
		// There is no need to recompute the element after this processor has executed
        return false;
	}

	@Override
	protected boolean removeAttributeIfEmpty(Arguments arguments,
			Element element, String attributeName, String newAttributeName) {
        return true;
	}


}
