package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class KeywordOccurence {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(nullable = false)
	private Long start;

	@Column(nullable = false)
	private Long end;

	@Column(length = 50, nullable = false)
	private String value;

	@Column(length = 300)
	private String imageUrl;

	public KeywordOccurence() {
		super();
	}

	public KeywordOccurence(String value) {
		super();
		this.value = value;
	}

	public KeywordOccurence(String value, Long start, Long end) {
		super();
		this.start = start;
		this.end = end;
		this.value = value;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStart() {
		return start;
	}

	public void setStart(Long start) {
		this.start = start;
	}

	public Long getEnd() {
		return end;
	}

	public void setEnd(Long end) {
		this.end = end;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String toString() {
		return "KeywordOccurence [id=" + id + ", start=" + start + ", end="
				+ end + ", value=" + value + ", imageUrl=" + imageUrl + "]";
	}

}
