package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.KeywordOccurence;

@Repository
public interface KeywordRepository extends
		CrudRepository<KeywordOccurence, Long> {

	public KeywordOccurence findByValue(String value);

}
