package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.thymeleaf.dialect;

import java.util.HashSet;
import java.util.Set;

import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.processor.IProcessor;

public class MenuDialect extends AbstractDialect {

	@Override
	public String getPrefix() {
		return "menu";
	}

	@Override
	public Set<IProcessor> getProcessors() {
		final Set<IProcessor> processors = new HashSet<IProcessor>();
        processors.add(new DecideMenuIsActiveProcessor());
        return processors;
	}
	
	

}
