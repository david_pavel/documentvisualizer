package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images;

import java.util.List;

public interface ImageService {
	public List<String> findImages(String query, Integer count, ImageType imageType);

	default public List<String> findImages(String query, ImageType imageType) {
		return findImages(query, 10, imageType);
	}

	default public String findImage(String query, ImageType imageType) {
		List<String> images = findImages(query, 1, imageType);
		if (images == null || images.size() == 0) {
			return null;
		}
		return images.get(0);
	}
}
