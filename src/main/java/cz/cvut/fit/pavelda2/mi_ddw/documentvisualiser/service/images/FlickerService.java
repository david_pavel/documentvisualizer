package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Source;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;
import org.springframework.xml.xpath.NodeMapper;
import org.springframework.xml.xpath.XPathOperations;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

@Service
@PropertySource("application.properties")
public class FlickerService implements ImageService {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Value("${flicker.api-key}")
	private String apiKey;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private XPathOperations xpathTemplate;

	/**
	 * Lookup image for the query on Flicker.
	 * 
	 * Search by image type not implemented.
	 */
	@Override
	public List<String> findImages(String query, Integer count, ImageType imageType) {
		final URI uri = createUri(apiKey, query, count);
		logger.info(MessageFormat.format("Search flicker images [query={0}, uri={1}]", query,uri));

		Source result = restTemplate.getForObject(uri, Source.class);

		return xpathTemplate.evaluate("//photo", result,
				new NodeMapper<String>() {
					public String mapNode(Node node, int nodeNum)
							throws DOMException {
						Element photo = (Element) node;
						Map<String, String> variables = new HashMap<String, String>(
								3);
						variables.put("server", photo.getAttribute("server"));
						variables.put("id", photo.getAttribute("id"));
						variables.put("secret", photo.getAttribute("secret"));

						return new UriTemplate(
								"http://static.flickr.com/{server}/{id}_{secret}_m.jpg")
								.expand(variables).toString();
					}
				});
	}

	private URI createUri(String apiKey, String tag, int size) {
		final String url = "https://www.flickr.com/services/rest?method=flickr.photos.search&api+key="
				+ apiKey + "&tags=" + tag.replaceAll("\\s", "%20") + "&per_page=" + size;
		try {
			return new URI(url);
		} catch (URISyntaxException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public String getType() {
		return "xml";
	}

}
