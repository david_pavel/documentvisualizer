package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service;

import java.util.List;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.Document;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.TextSource;



public interface DocumentService {
	public Document storeText(TextSource textSource);
	public List<Document> getAllDocumentsByCategory(String categoryName);
	public List<Document> getAllDocuments();
	public Document getById(Long id);
}
