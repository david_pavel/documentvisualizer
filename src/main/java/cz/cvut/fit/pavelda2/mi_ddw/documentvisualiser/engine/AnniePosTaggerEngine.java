package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.ProcessingResource;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.KeywordOccurence;

@Component
public class AnniePosTaggerEngine implements KeywordsExtractionEngine {
	private static final String FILTER_TYPE = "Token";
	private final String PIPELINE_RESOURCE = "gate.creole.SerialAnalyserController";
	private final String[] RESOURCES = {
			"gate.creole.annotdelete.AnnotationDeletePR",
			"gate.creole.tokeniser.DefaultTokeniser",
			"gate.creole.splitter.SentenceSplitter", "gate.creole.POSTagger" };
	private final FeatureMap NOUNS_FEATURE_MAP;

	private SerialAnalyserController pipeline;

	public AnniePosTaggerEngine() {
		try {
			this.pipeline = (SerialAnalyserController) Factory
					.createResource(PIPELINE_RESOURCE);
			for (String resource : RESOURCES) {
				pipeline.add((ProcessingResource) Factory
						.createResource(resource));
			}
		} catch (ResourceInstantiationException e) {
			throw new IllegalStateException(e);
		}

		NOUNS_FEATURE_MAP = Factory.newFeatureMap();
		NOUNS_FEATURE_MAP.put("category", "NN");
		NOUNS_FEATURE_MAP.put("category", "NNP");
		NOUNS_FEATURE_MAP.put("category", "NNS");
	}

	@Override
	public List<KeywordOccurence> extractKeywords(String text) {
		Corpus corpus;
		try {
			corpus = Factory.newCorpus("");
			corpus.add(Factory.newDocument(text));
			pipeline.setCorpus(corpus);
			pipeline.execute();
		} catch (ResourceInstantiationException | ExecutionException e) {
			throw new IllegalStateException(e);
		}

		Document doc = corpus.get(0);
		AnnotationSet annotationSet = doc.getAnnotations().get(FILTER_TYPE,
				NOUNS_FEATURE_MAP);
		ArrayList<Annotation> tokenAnnotations = new ArrayList<Annotation>(
				annotationSet);
		List<KeywordOccurence> keywords = new ArrayList<>();
		for (int j = 0; j < tokenAnnotations.size(); j++) {
			Annotation token = tokenAnnotations.get(j);
			String value = token.getFeatures().get("string").toString();
			Long start = token.getStartNode().getOffset();
			Long end = token.getEndNode().getOffset();
			keywords.add(new KeywordOccurence(value, start, end));
		}

		return keywords;
	}

}
