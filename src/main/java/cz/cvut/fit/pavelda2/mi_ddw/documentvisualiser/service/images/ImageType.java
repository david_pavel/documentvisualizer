package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images;

public enum ImageType {
	CLIPART,
	PHOTO,
	LINEART,
	FACE
}
