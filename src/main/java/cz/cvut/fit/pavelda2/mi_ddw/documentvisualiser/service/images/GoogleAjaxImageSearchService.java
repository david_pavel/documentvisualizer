package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Primary
public class GoogleAjaxImageSearchService implements ImageService {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private static final ImageType DEFAULT_IMAGE_TYPE = ImageType.CLIPART;

	@Override
	public List<String> findImages(String query, Integer count, ImageType imageType) {
		ImageType currentImageType = imageType == null ? DEFAULT_IMAGE_TYPE : imageType;
		try {
			Map<String,String> params = new HashMap<>();
			params.put("q", query);
			params.put("v", "1.0");
			params.put("imgtype", currentImageType.name().toLowerCase());
			
			
			URL url = new URL("https://ajax.googleapis.com/ajax/services/search/images?" +
	                encodeParams(params));
			logger.debug("Requesting images form URL resource: " + url);
			URLConnection connection = url.openConnection();
			
			String line;
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while((line = reader.readLine()) != null) {
				builder.append(line);
			}
			
			List<String> images = new ArrayList<String>();
			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readValue(builder.toString(),JsonNode.class);
			JsonNode results = rootNode.get("responseData").get("results");
			for(int i = 0; i < results.size(); i++) {
				String imgUrl = results.get(i).get("url").asText();
				logger.trace("Retrieved image: " + imgUrl + ", for params: " + Arrays.toString(params.entrySet().toArray()));
				images.add(imgUrl);
			}
			return images;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String encodeParams(Map<String, String> params) {
		List<NameValuePair> namedValuePairs = params.entrySet()
				.stream()
				.map((e) -> new BasicNameValuePair(e.getKey(), e.getValue()))	
				.collect(Collectors.toList());	
		
		return URLEncodedUtils.format(namedValuePairs, "UTF-8");
	}
	
}
