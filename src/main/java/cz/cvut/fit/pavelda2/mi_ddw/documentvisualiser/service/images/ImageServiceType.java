package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images;

public enum ImageServiceType {
	FLICKER(FlickerService.class),
	GOOGLE_IMAGES(GoogleAjaxImageSearchService.class);
	
	private Class<? extends ImageService> imageServiceClass;

	private ImageServiceType(Class<? extends ImageService> imageServiceClass) {
		this.imageServiceClass = imageServiceClass;
	}

	public Class<? extends ImageService> getImageServiceClass() {
		return imageServiceClass;
	}
	
	
}
