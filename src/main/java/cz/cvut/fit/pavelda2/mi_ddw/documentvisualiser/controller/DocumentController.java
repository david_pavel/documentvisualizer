package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.Document;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.TextSource;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.TextSourceFactory;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine.KeywordsExtractionEngineType;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine.KeywordsExtractor;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.DocumentService;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images.ImageServiceType;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images.ImageType;

@Controller
public class DocumentController {
	private final Logger logger = LoggerFactory.getLogger(getClass());  
	
	@Autowired
	private KeywordsExtractor extractor;
	
	@Autowired
	private DocumentService documentService;
	

	@RequestMapping(value="/Documents",method=RequestMethod.GET)
	public String listDocuments(Model model) {
		List<Document> allDocuments = documentService.getAllDocuments();
		model.addAttribute("documents",allDocuments);
		return "/document/list";
	}	

	@RequestMapping(value="/CreateDocument", method=RequestMethod.GET)
	public String createDocument(Model model) {
		model.addAttribute("textSource", TextSourceFactory.createBasicTextSource());
		model.addAttribute("extractorTypes",KeywordsExtractionEngineType.values());
		model.addAttribute("imageServiceTypes",ImageServiceType.values());
		model.addAttribute("imageTypes",ImageType.values());
		return "/document/create";
	}	
	
	@RequestMapping(value="/CreateDocument",method=RequestMethod.POST)
	public String acceptDocument(@ModelAttribute("textSource") TextSource textSource, Model model) {
		logger.info("Creating document form text source: " + textSource.toString());
		Document document = documentService.storeText(textSource);		
		return "redirect:/Documents/" + document.getId();
	}	
	
	@RequestMapping(value="/Documents/{id}",method=RequestMethod.GET)
	public String viewDocument(@PathVariable("id") Document document, Model model) {
		
		model.addAttribute("document",document);
		return "/document/view";
	}	
}
