package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine.KeywordsExtractionEngineType;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images.ImageServiceType;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images.ImageType;

public class TextSourceFactory {

	public static TextSource createBasicTextSource() {
		TextSource textSource = new TextSource();
		textSource.setCategory("News");
		textSource.setExtractorTypes(KeywordsExtractionEngineType.OPEN_NLP_NER_ENGINE);
		textSource.setName("<Unknown>");
		textSource.setImageService(ImageServiceType.GOOGLE_IMAGES);
		textSource.setImageType(ImageType.CLIPART);
		return textSource;
	}

}
