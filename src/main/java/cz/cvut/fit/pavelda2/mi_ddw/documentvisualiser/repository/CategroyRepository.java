package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.repository;

import org.springframework.data.repository.CrudRepository;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.Category;

public interface CategroyRepository extends CrudRepository<Category, Long> {
	public Category findByName(String categoryName);

}
