package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.Category;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.Document;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.KeywordOccurence;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.TextSource;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine.KeywordsExtractor;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.repository.CategroyRepository;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.repository.DocumentRepository;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.repository.KeywordRepository;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images.ImageService;

@Service
@Transactional(readOnly = true)
public class DocumentServiceImpl implements DocumentService {
	final Logger logger = LoggerFactory.getLogger(getClass());

	private static final int IMAGES_LIMIT = 100;
	@Autowired
	private DocumentRepository documentRepository;
	@Autowired
	private CategroyRepository categoryRepository;
	@Autowired
	private KeywordRepository keywordRepository;

	@Autowired
	private KeywordsExtractor keywordsExtractor;
	
	@Autowired
	private ApplicationContext ctx;

	@Override
	@Transactional(readOnly = false)
	public Document storeText(TextSource textSource) {
		Category category = categoryRepository.findByName(textSource.getCategory());
		if (category == null) {
			category = categoryRepository.save(new Category(textSource.getCategory()));
		}

		List<KeywordOccurence> keywords = keywordsExtractor.extractKeywords(
				textSource.getText(), Arrays.asList(textSource.getExtractorTypes()));

		ImageService imagesService = ctx.getBean(textSource.getImageService().getImageServiceClass());
		logger.debug("Using image service class of type: " + imagesService.getClass());
		
		keywords
			.parallelStream()
			.limit(IMAGES_LIMIT)
			.forEach(k -> k.setImageUrl(
					imagesService.findImage(k.getValue(), textSource.getImageType())));
		Document document = new Document(textSource.getName(),
				textSource.getText(), textSource.getUrl(), category, keywords);

		return documentRepository.save(document);
	}

	@Override
	public List<Document> getAllDocumentsByCategory(String categoryName) {
		return documentRepository.findAllByCategoryName(categoryName);
	}

	@Override
	public List<Document> getAllDocuments() {
		return documentRepository.findAll();
	}

	@Override
	public Document getById(Long id) {
		return documentRepository.findOne(id);
	}

}
