package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine.KeywordsExtractionEngineType;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images.ImageServiceType;
import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.service.images.ImageType;

public class TextSource {
	private String text;
	private String name;
	private String url;
	private String category;
	private KeywordsExtractionEngineType[] extractorTypes;
	private ImageServiceType imageService;
	private ImageType imageType;

	public TextSource() {
		super();
	}

	public TextSource(KeywordsExtractionEngineType ... extractorTypes) {
		this.extractorTypes = extractorTypes;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public KeywordsExtractionEngineType[] getExtractorTypes() {
		return extractorTypes;
	}
	
	public void setExtractorTypes(KeywordsExtractionEngineType ... extractorTypes) {
		this.extractorTypes = extractorTypes;
	}

	public ImageServiceType getImageService() {
		return imageService;
	}

	public void setImageService(ImageServiceType imageService) {
		this.imageService = imageService;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

		
	public ImageType getImageType() {
		return imageType;
	}

	public void setImageType(ImageType imageType) {
		this.imageType = imageType;
	}

	@Override
	public String toString() {
		return "TextSource [text=" + text + ", url=" + url + ", category="
				+ category + ", extractorTypes=" + extractorTypes + "]";
	}

}
