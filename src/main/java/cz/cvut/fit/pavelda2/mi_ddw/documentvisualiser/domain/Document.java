package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

@Entity
public class Document {

	private static final Comparator<KeywordOccurence> keywordsComparator = (k1,
			k2) -> k1.getValue().compareToIgnoreCase(k2.getValue());

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Type(type = "text")
	@Column(nullable = false)
	private String text;
	
	@Column(nullable = false, length=60)
	private String name;

	@Column(length = 300)
	private String url;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private Category category;

	@OneToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	private List<KeywordOccurence> keywords = new ArrayList<>();

	@Transient
	private Map<KeywordOccurence, Integer> keywordsMap;

	public Document() {
		super();
	}

	public Document(String name, String text, String url, Category category,
			List<KeywordOccurence> keywords) {
		super();
		this.name = name;
		this.text = text;
		this.url = url;
		this.category = category;
		this.keywords = keywords;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<KeywordOccurence> getKeywords() {
		return keywords;
	}
	
	public Map<String, Integer> getOccurences() {
		Map<String, Integer> map = new HashMap<>();
		// group by occurrences count
		for (KeywordOccurence kw : keywords) {
		    map.compute(kw.getValue(), (k, v) -> (v == null) ? 1 : v + 1);
		}
		// sort
		return map.entrySet().stream()
			.sorted((e1,e2) 
				-> e1.getValue().equals(e2.getValue()) 
				? e1.getKey().compareTo(e2.getKey()) 
				: e1.getValue().compareTo(e2.getValue()))
			.collect(Collectors.toMap((e)-> e.getKey(),(e)-> e.getValue()));
	}

	public void setKeywords(List<KeywordOccurence> keywords) {
		this.keywords = keywords;
	}
		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<KeywordOccurence,Integer> getKeywordsMap() {
		if (keywordsMap == null) {
			keywordsMap = new TreeMap<KeywordOccurence, Integer>(keywordsComparator);
			keywords.stream().forEach(k -> {
				Integer count = keywordsMap.get(k);
				keywordsMap.put(k, count == null ? 1 : count + 1);
			});
		}
		return keywordsMap;
	}

	@Override
	public String toString() {
		return "TextSource [text=" + text + ", url=" + url + ", category="
				+ category + ", keywords=" + keywords + "]";
	}

}
