package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.ProcessingResource;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.KeywordOccurence;

@Component
public class OpenNlpNerEngine implements KeywordsExtractionEngine {
	private final String PIPELINE_RESOURCE = "gate.creole.SerialAnalyserController";
	private final String[] RESOURCES = {
			"gate.creole.annotdelete.AnnotationDeletePR",
			"gate.creole.tokeniser.DefaultTokeniser",
			"gate.creole.splitter.SentenceSplitter",
			"gate.opennlp.OpenNLPNameFin" };

	private SerialAnalyserController pipeline;

	public OpenNlpNerEngine() {
		try {
			this.pipeline = (SerialAnalyserController) Factory
					.createResource(PIPELINE_RESOURCE);
			for (String resource : RESOURCES) {
				pipeline.add((ProcessingResource) Factory
						.createResource(resource));
			}
		} catch (ResourceInstantiationException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public List<KeywordOccurence> extractKeywords(String text) {
		Corpus corpus;
		try {
			corpus = Factory.newCorpus("");
			corpus.add(Factory.newDocument(text));
			pipeline.setCorpus(corpus);
			pipeline.execute();
		} catch (ResourceInstantiationException | ExecutionException e) {
			throw new IllegalStateException(e);
		}

		Document doc = corpus.get(0);
		AnnotationSet annotationSet = doc.getAnnotations();
		ArrayList<Annotation> tokenAnnotations = new ArrayList<Annotation>(
				annotationSet);
		List<KeywordOccurence> keywords = new ArrayList<>();

		tokenAnnotations
				.stream()
				.filter(t -> t.getFeatures().containsKey("source"))
				.forEach(
						(t) -> {
							Long start = t.getStartNode().getOffset();
							Long end = t.getEndNode().getOffset();
							String value = null;
							try {
								value = doc.getContent().getContent(start, end)
										.toString();
							} catch (Exception e) {
							}
							keywords.add(new KeywordOccurence(value, start, end));
						});

		return keywords;
	}

}
