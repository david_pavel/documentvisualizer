package cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.engine;

import java.util.List;

import cz.cvut.fit.pavelda2.mi_ddw.documentvisualiser.domain.KeywordOccurence;

public interface KeywordsExtractor {
	public List<KeywordOccurence> extractKeywords(String text, List<KeywordsExtractionEngineType> engineTypes);
}
