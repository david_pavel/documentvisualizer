function trimEmptyStringToNull(obj) {
	
	 $.each(obj, function(key, value){
		 if (value === "" || value === null){
	        delete obj[key];
		 } else if (Object.prototype.toString.call(value) === '[object Object]') {
         	trimEmptyStringToNull(value);			 
		 } else if ($.isArray(value)) {
            $.each(value, function (k,v) { 
            	if (v === "" || v === null) {
            		v = undefined;
            	} else if (Object.prototype.toString.call(v) === '[object Object]') {
            		trimEmptyStringToNull(v);
            	}
        	});
        }
	 });	
	
    return obj;
}

function createLogLevelLabel(data) {
	var labelType;
	switch(data) {
		case 'INFO':
			labelType = 'success';
			break;
		case 'WARN':
			labelType = 'warning';
			break;
		case 'ERROR':
			labelType = 'danger';
			break;
		default:
			labelType = 'default';
			break;
	}
	return '<span class="label label-' + labelType + '">' + data + '</span>';
}

$.fn.clearForm = function() {
	var $form = $(this);

	// Bootstrap checkboxes, radio buttons
	$form.find('label.active').trigger("click");
	
	$form.find('select').selectpicker('deselectAll');
	$form.find('select').each(function() {
		$(this).selectpicker('val', '');
		$(this).selectpicker('render');
	});

		
	// Input fields
	//$form.trigger("reset"); we want to clear the form, not to reset it
	$(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
}

function doCenterElement() {    
	var footerHeight = $('#footer').height();
	var headerHeight = $('#header .navbar').height();
	var height = $(this).height();
    var offset = ($(window).height() - height) / 2 - footerHeight - headerHeight;
	 $(this).css("margin-top", offset);
}


$.fn.centerElement = function() {
	$(window).on("resize", function () {
		doCenterElement.apply($(this));
	});
	doCenterElement.apply($(this));
}

String.prototype.htmlEscape = function() {
    return $('<div/>').text(this.toString()).html();
  };