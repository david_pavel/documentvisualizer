# README #

### What is this repository for? ###

* Web application for visualizing text documents using its keywords.
* Version: 0.0.1
* [Project Wiki](https://bitbucket.org/david_pavel/documentvisualizer/wiki)

### How do I get set up? ###

For application execution following external libraries are required:

* Gate, including ANNIE and OpenNLP plugins (Gate location can be set in application.properties named 'gate.home')

Base configuration

* Default database is HSQLDB (file storage ~/hsqldb/documentVisualizer) or you can use your favourite DB (for DB connection configuration see application.properties)